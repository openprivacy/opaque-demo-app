.PHONY: all clean linux windows android
all: clean linux windows android
default: linux 

clean:
	rm -r vendor || true
	find -type f -iname "moc*" | xargs rm
	find -iname "rcc*" | xargs rm

linux:
	$(MAKE) linux_build || $(MAKE) linux_clean

windows:
	$(MAKE) windows_build || $(MAKE) windows_clean

android:
	$(MAKE) android_build || $(MAKE) android_clean

linux_build:
	date
	qtdeploy -docker -qt_version "5.13.0" build linux 2>&1 | tee qtdeploy.log | pv
	date
	cp -R assets deploy/linux/
	$(MAKE) linux_clean

linux_clean:
	#ntd

windows_build:
	date
	qtdeploy -qt_version "5.13.0" build windows 2>&1 | tee qtdeploy.log | pv
	date
	cp -R assets deploy/windows/
	$(MAKE) linux_clean

windows_clean:
	#ntd

android_build:
	mv assets android/
	date
	qtdeploy -docker build android 2>&1 | tee qtdeploy.log | pv
	date
	$(MAKE) android_clean

android_clean:
	mv android/assets assets

