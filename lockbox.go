package main

import (
	"flag"
	"git.openprivacy.ca/openprivacy/lockbox/api"
	"git.openprivacy.ca/openprivacy/lockbox/uilib"
	"git.openprivacy.ca/openprivacy/log"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/qml"
	"github.com/therecipe/qt/quickcontrols2"
	"github.com/therecipe/qt/widgets"
	"os"
)

var (
	flagLocal, flagDebug *bool
	flagDataDir *string
)

func main() {
	flagDataDir = flag.String("dir", "data", "working directory")
	flagLocal = flag.Bool("local", false, "load qml from the local ./qml/ directory instead of from bundled resources (useful for developers)")
	flagDebug = flag.Bool("debug", false, "additional debug output on console")
	flag.Parse()

	if *flagDebug {
		log.SetLevel(log.LevelDebug)
	}

	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)
	app := widgets.NewQApplication(len(os.Args), os.Args)
	app.SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)
	quickcontrols2.QQuickStyle_SetStyle("Default")
	engine := qml.NewQQmlApplicationEngine(nil)

	lockapi := api.NewLockBoxAPI(nil)
	engine.RootContext().SetContextProperty("lockbox", lockapi)

	fdm := NewFormDataModel(nil)
	BuildFormModel(fdm, *flagDataDir)
	engine.RootContext().SetContextProperty("forms", fdm)

	theme := uilib.NewThemeModel(nil)
	theme.SetAssetPath("assets/")
	theme.SetThemeScale(2.0)
	engine.RootContext().SetContextProperty("gcd", theme)

	if *flagLocal {
		engine.Load(core.QUrl_FromLocalFile("./qml/main.qml"))
	} else {
		engine.Load(core.NewQUrl3("qrc:/qml/main.qml", 0))
	}

	widgets.QApplication_Exec()
}