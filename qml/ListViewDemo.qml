import QtQuick 2.7 //ApplicationWindow
import QtQuick.Controls 2.1 //Dialog
import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
import QtQuick.Controls.Styles 1.4

import "opaque/theme"
import "opaque" as Widgets
import "opaque/fonts"

ColumnLayout {
	spacing: 24

	Widgets.ScalingLabel {
		text: "QtQuick.Controls.Listview 2.x"
	}

	ListView {
		Layout.fillHeight: true
		Layout.fillWidth: true
		model: forms
		clip: true
		spacing: 12

		delegate: Item {
			implicitWidth: parent.width
			implicitHeight: 80

			Rectangle {
				anchors.fill: parent
				border.color: "black"
				border.width: 1
				color: index % 2 == 0 ? Theme.backgroundPaneColor : Theme.backgroundMainColor
				radius: 12
			}

			Widgets.ScalingLabel {
				leftPadding: 12
				topPadding: 6
				text: (FormName == "" ? "(untitled)" : FormName) + "\n" + FolderName
			}
		}
	}
}