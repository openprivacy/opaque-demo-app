import QtQuick 2.7 //ApplicationWindow
import QtQuick.Controls 2.1 //Dialog
import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
//import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4 as C

import "opaque/theme"
import "opaque" as Widgets
import "opaque/fonts"

ApplicationWindow {
    id: root
    visible: true
    title: "OPQT QAbstract_Model Generator/Demo"
    minimumWidth: 800
    minimumHeight: 640
	background: Rectangle {
		color: Theme.backgroundPaneColor
		Behavior on color {
			ColorAnimation { duration: 400 }
		}
	}
    font.family: Fonts.applicationFontRegular.name
    font.styleName: "Bold"

	RowLayout {
		anchors.fill: parent

		ColumnLayout {
			Widgets.Button {
				text: "TableView Demo"
				onClicked: mainLoader.source = "TableViewDemo.qml"
			}

			Widgets.Button {
				text: "ListView Demo"
				onClicked: mainLoader.source = "ListViewDemo.qml"
			}

			Widgets.ToggleSwitch {
				onToggled: function(){
					Theme.theme = isToggled ? Theme.light : Theme.dark;
				}
			}
		}

		Loader {
			id: mainLoader
			Layout.fillWidth: true
			Layout.fillHeight: true
			Layout.margins: 24
			source: "TableViewDemo.qml"
		}
	}

    MessageDialog {
        id: messageDialog
        title: "May I have your attention please"
        text: "It's so cool that you are using Qt Quick."
        onAccepted: {
            messageDialog.close()
        }
        Component.onCompleted: visible = false
    }
}
