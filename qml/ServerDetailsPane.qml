import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import Qt.labs.folderlistmodel 2.12
import "qml-widgets" as Widgets

Column {
	padding: 12
	spacing: 12

	//===\\

	Widgets.ScalingLabel {
		size: 32
		text: lv.myData.FormName
	}

	//===\\

	Widgets.ScalingLabel {
		visible: lv.myData.Error != ""
		color: "#FF0000"
		text: lv.myData.Error
	}

	//===\\

	Widgets.ScalingLabel {
		text: "Folder"
	}

	Widgets.ButtonTextField {
		id: txtFolder
		text: lv.myData.FolderName
		width: parent.width * 0.95
		button_text: "Open"
	}

	Widgets.Button {
		text: "Open Submissions Directory"
	}

	//===\\

	Widgets.ScalingLabel {
		text: "API URL"
	}

	Widgets.TextField {
		id: txtAPI
		text: lv.myData.API
		width: parent.width * 0.95
	}

	//===\\

	Widgets.ScalingLabel {
		text: "Public URL"
	}

	Widgets.ButtonTextField {
		text: txtAPI.text + lv.myData.FormID
		width: parent.width * 0.95
		button_text: "Open"
	}

	//===\\

	Widgets.ScalingLabel {
		text: "Admin URL"
	}

	Widgets.ButtonTextField {
		text: txtAPI.text + "admin"
		width: parent.width * 0.95
		button_text: "Open"
	}

	//===\\

	Widgets.ScalingLabel {
		text: "Username"
	}

	Widgets.TextField {
		id: txtUsername
		text: lv.myData.User
		width: parent.width * 0.5
	}

	//===\\

	Widgets.ScalingLabel {
		text: "Public Key"
	}

	Column {
		visible: lv.myData.LocalPubKey == ""

		Widgets.ScalingLabel { text: "Not yet created" }
		Widgets.Button { text: "Generate" }
	}

	Column {
		visible: lv.myData.LocalPubKey != "" && lv.myData.RemotePubKey == ""

		Widgets.ScalingLabel { text: "Generated but not yet uploaded" }
		Widgets.Button { text: "Upload" }
	}

	Column {
		visible: lv.myData.RemotePubKey != "" && lv.myData.RemotePubKey != lv.myData.LocalPubKey

		Widgets.ScalingLabel { text: "Error: Remote encryption key does not match local public.key file" }
	}

	Column {
		visible: lv.myData.RemotePubKey != "" && lv.myData.RemotePubKey == lv.myData.LocalPubKey

		Widgets.ScalingLabel { text: "Uploaded successfully!" }
		Widgets.ScalingLabel { text: "Submissions" }
		Widgets.ScalingLabel { text: lv.myData.Submissions }
	}

	//===\\
}