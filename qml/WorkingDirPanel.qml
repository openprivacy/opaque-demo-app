import QtQuick 2.7 //ApplicationWindow
import QtQuick.Controls 2.1 //Dialog
import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import "qml-widgets" as Widgets

Column {
	id: root
	property alias cwd: txtWorkingDirectory.text

	Image {
		width: parent.width
		fillMode: Image.PreserveAspectFit
		source: "qrc:/qml/lockbox.png"
	}

	Row {
		spacing: 12
		anchors.horizontalCenter: parent.horizontalCenter

		Widgets.ScalingLabel {
			text: "Working directory:"
		}

		Widgets.ButtonTextField {
			id: txtWorkingDirectory
			width: root.width * 0.67
			text: forms.cwd
			button_text: "Browse..."
		}
	}
}