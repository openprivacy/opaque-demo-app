import QtQuick 2.7 //ApplicationWindow
import QtQuick.Controls 2.1 //Dialog
import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12

import "qml-widgets" as Widgets

Column {
	padding: 12
	spacing: 12

	Widgets.ScalingLabel {
		text: "Option 1: Paste a token from a lockbox website"
	}

	Widgets.TextField {
		text: "eirughliser+Stg..."
		width: parent.width * 0.9
	}

	Widgets.Button {
		text: "Connect"
	}

	Item {
		width: 1
		height: 20
	}

	Widgets.ScalingLabel {
		text: "Option 2: Just generate encryption keys\n(Warning: you won't be able to connect to a website later)"
	}

	Row {
		Widgets.ScalingLabel {
			text: "Form name: "
		}

		Widgets.TextField {
			id: txtNewFormName
		}

		Widgets.Button {
			text: "Generate"
			onClicked: forms.createLocalFormEntry(txtNewFormName.text)
		}
	}
}