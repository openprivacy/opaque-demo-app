module git.openprivacy.ca/openprivacy/lockbox

go 1.13

require (
	git.openprivacy.ca/openprivacy/log v1.0.0
	github.com/therecipe/qt v0.0.0-20200126204426-5074eb6d8c41
	github.com/therecipe/qt/internal/binding/files/docs/5.12.0 v0.0.0-20200126204426-5074eb6d8c41 // indirect
	github.com/therecipe/qt/internal/binding/files/docs/5.13.0 v0.0.0-20200126204426-5074eb6d8c41 // indirect
	golang.org/x/crypto v0.0.0-20200320181102-891825fb96df
)
