package uilib

import "github.com/therecipe/qt/core"

type ThemeModel struct {
	core.QObject

	_ string `property:"assetPath"`
	_ float32 `property:"themeScale"`
}